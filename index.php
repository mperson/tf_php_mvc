<?php
@session_start();
require("./tools/sanitize.inc.php");
include("./controllers/account.controller.php"); 
include("./controllers/home.controller.php"); 
$ctl="home";
$action="index";
if(isset($_GET["controller"]))
{
   $ctl= $_GET["controller"];
}
if(isset($_GET["action"]))
{
   $action= $_GET["action"];
}
try 
{
    $classname=$ctl."Controller"; 
    $obj = new $classname; // new homeController();
   
    call_user_func([$obj,$action]); // $obj->Index
}
catch(Exception $ex)
{
    $obj= new homeController();
    $obj->Index();
}


?>