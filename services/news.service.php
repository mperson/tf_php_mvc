<?php
require("./models/news.model.php");
class newsservice
{ 
    private $pdo;
    function __construct($pdo)
    {
       $this->pdo=$pdo;    
    }

    public function getAll()
    {
        $result= $this->pdo->query("Select * from news order by Publication");
        $result->setFetchMode(PDO::FETCH_CLASS, 'news');
        return $result;
    }
    public function getOne($id)
    { 
        if(!filter_var($id,FILTER_VALIDATE_INT)) throw new Exception("id must be an integer");
        $prepare= $this->pdo->prepare("Select * from news where IdNews=:id order by Publication");        
        $prepare->setFetchMode(PDO::FETCH_CLASS, 'news');
        $result = $prepare->execute(array('id'=>$id));
        if(!$result) return null;
        else
        return $prepare->fetch();
    }
    
    public function insert($newevent)
    { 
        if(!$newevent instanceof news) throw new Exception("parameter must be an instance of news class");
        $prepare= $this->pdo->prepare("insert into news (Titre,Contenu,Photo,Publication) values (:Titre, :Contenu, :Photo, :Publication)");         
        $result=$prepare->execute((array)$newevent);
        return $result;
    }
}