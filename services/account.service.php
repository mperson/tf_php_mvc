<?php
include("./models/user.model.php");
class accountService
{

    private $pdo;
    function __construct($pdo)
    {
       $this->pdo=$pdo;    
    }

    public function insert($newUser)
    { 
        if(!$newUser instanceof user) throw new Exception("parameter must be an instance of user class");
        $prepare= $this->pdo->prepare("insert into user (Login, Password) values (:Login, :Password)");         
        $result=$prepare->execute((array)$newUser);
        return $result;
    }

    public function getByLogin($login)
    {   
            $prepare= $this->pdo->prepare("Select * from user where Login=:login");        
            $prepare->setFetchMode(PDO::FETCH_CLASS, 'user');
            $result = $prepare->execute(array('login'=>$login));
            if(!$result) 
               return null;
            else
            return $prepare->fetch(); 
    }
}
?>