<?php
require_once("./config/config.conf.php");

function PDOConnect()
{
    try
    {
        $pdo= new PDO("mysql:host=".SERVER.";port=3306;dbname=".DATABASE, LOGIN,PASS);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $pdo;
    }
    catch(PDOException $ex)
    {
        var_dump($ex->getMessage());
        die();
    }
}