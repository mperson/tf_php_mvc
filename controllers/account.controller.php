<?php
require_once("./data/db.inc.php");
require_once("./services/account.service.php");
require_once("./models/user.model.php");

class accountController
{
   function registerOrLogin()
   {
    include "./views/account/registerorlogin.view.php";
   }

    function register()
    {
        $user = $_POST["user"];
        $pass= $_POST["password"];

        //1- hasher mon password
        $hashPassword = password_hash($pass, PASSWORD_DEFAULT );

        $pdo = PDOConnect();
        $acc = new accountService($pdo);

        $userModel = new user();
        $userModel->Login=$user;
        $userModel->Password=$hashPassword;

        if($acc->insert($userModel))
        {
            include("./views/home/merci.view.php");
        }

    }

    function logout()
    {
        session_destroy();
        header("Location: index.php?controller=home&action=index");
    }

    function login()
    {
        $user = $_POST["user"];
        $pass= $_POST["password"];
        //Attention l'appel de cette fonction ne produit pas le même résultat qu'en ligne 14
        //$hashPassword = password_hash($pass, PASSWORD_DEFAULT );
        
        //on doit utiliser password_verify
        //1 récupérer le user
        $pdo = PDOConnect();
        $acc = new accountService($pdo);
        $userModel = $acc->getByLogin($user);  
        if(password_verify($pass, $userModel->Password))
        {
            //ok, tu es loggé
            $_SESSION["isLogged"]=true;
            $_SESSION["Name"]= $user;          
            header("Location: index.php?controller=home&action=index");
        }
        else
        {
            //ko pas loggé
            header("Location: http://localhost:8070/cms/index.php?controller=account&action=registerorlogin&error=login failed");
        }


    }
}
?>