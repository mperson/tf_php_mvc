<?php
require_once("./config/config.conf.php");
require_once("./data/db.inc.php");
require_once("./services/news.service.php");
require_once("./services/mail.service.php");
class homeController
{
    function Index()
    { 

        $pdo = PDOConnect();
        $newService = new newsservice($pdo);
        $news = $newService->getall(); 
        require("./views/home/index.view.php");
    }

    function Details()
    {
        if(!isset($_GET["id"])) throw new Exception("NOT FOUND");
        $id=$_GET["id"];
        $pdo = PDOConnect();
        $newService = new newsservice($pdo);
        $news = $newService->getOne($id); 
        require("./views/home/detail.view.php");
    }

    function About()
    {
        require("./views/home/about.view.php");
    }

   function contactForm()
   {
    require("./views/home/contact.view.php");
   }

    function Contact()
    {
        if($_SERVER["REQUEST_METHOD"]!="POST") throw new Exception("NOT FOUND");

            $body="
            Message de : ".$_POST['name']."
            <br>
            Email  : ".$_POST["email"]."
            <hr>
            Message :<br>
            ".nl2br($_POST["message"])."
            ";


        //mail a envoyer
        $mService = new mailService(SMTPSERVER,SMTPPORT,SMTPUSER,SMTPPASS);
        $mService->sendMail("test@test.be","Message à partir de votre site : ".$_POST["subject"], $body);
        
        require("./views/home/merci.view.php");
    }
}