
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title>PHP News</title>

<!--

Breezed Template

https://templatemo.com/tm-543-breezed

-->
    <!-- Additional CSS Files -->
    <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="public/css/font-awesome.css">

    <link rel="stylesheet" href="public/css/templatemo-breezed.css">

    <link rel="stylesheet" href="public/css/owl-carousel.css">

    <link rel="stylesheet" href="public/css/lightbox.css">

    </head>
    
    <body>
     <!-- ***** Preloader Start 
     <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  ***** -->
    <!-- ***** Preloader End ***** -->
    
    
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav class="main-nav">
                        <!-- ***** Logo Start ***** -->
                        <a href="index.html" class="logo">
                           PHP News
                        </a>
                        <!-- ***** Logo End ***** -->
                        <!-- ***** Menu Start ***** -->
                        <ul class="nav">
                            <li class="scroll-to-section"><a href="/" class="active">Home</a></li>
                            <li class="scroll-to-section"><a href="index.php?controller=home&action=about">About</a></li> 
                            <li class="scroll-to-section"><a href="index.php?controller=home&action=contactForm">Contact Us</a></li> 
                            <?php
                            if(!isset($_SESSION["isLogged"]) || $_SESSION["isLogged"]==false)
                            {?>
                                <li class="scroll-to-section"><a href="index.php?controller=account&action=registerorlogin">Enregistrement/Login</a></li>
                            <?php
                            }
                            else
                            {?>
                                 <li class="scroll-to-section"><a href="index.php?controller=account&action=logout" class="btn-danger"><?=$_SESSION["Name"]?>(Logout)</a></li>
                            <?php
                            }                            
                            ?>
                           
                        </ul>        
                        <a class='menu-trigger'>
                            <span>Menu</span>
                        </a>
                        <!-- ***** Menu End ***** -->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->