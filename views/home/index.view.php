<?php
    include("./views/shared/header.layout.php");

     
    ?> 
    <section class="section" id="projects">
      <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="section-heading">
                    <h6>Les dernières news</h6>
                    <h2>(10 dernières news créées)</h2>
                </div>
                <div class="filters">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        <li data-filter=".des">Web Design</li>
                        <li data-filter=".dev">Web Development</li>
                        <li data-filter=".gra">Graphics</li>
                        <li data-filter=".tsh">Artworks</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="filters-content">
                     
                    <div class="row grid">
                   <?php
                    while($current=$news->fetch())
                    {
                        $datePub=date_format(new DateTime($current->Publication), "d-m-Y");                         
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 all des">
                          <div class="item">
                            <a href="public/images/<?=$current->Photo?>" data-lightbox="<?=$current->Photo?>" data-title="News"><img src="public/images/<?=$current->Photo?>" height="200px" alt="">
                            <br> 
                            <p><b><?=$current->Titre?>(<?=$datePub?>)</b></p>                            
                            </a>
                            <hr>    
                            <?=$current->Contenu?><br>
                            <a class="btn btn-info" href="index.php?controller=home&action=details&id=<?=$current->IdNews?>">Détails</a>
                          </div>
                        </div> 
                    <?php
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section> 
    <?php
    include("./views/shared/footer.layout.php");
    ?>